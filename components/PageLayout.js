import Head from "next/head"

const PageLayout = (props) => {
    return(
      <>
      <Head>
      <meta charset="UTF-8" />
      <meta name="description" content="Financial Management Tool" />
      <meta name="keywords" content="Budgeting, Manage, Finance" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.googlestatic.com" crossOrigin="true" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
      <title>Rally</title>
      </Head>

      <div>
          {props.children}
      </div>
      </>
    )
}

export default PageLayout